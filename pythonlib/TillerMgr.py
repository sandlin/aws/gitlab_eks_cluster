import kubernetes
from kubernetes.client.rest import ApiException
import logging
import sys
import yaml
logger = logging.getLogger(__name__)

class TillerMgr(object):

    def __init__(self, namespace):
        self.namespace = namespace
        kubernetes.config.load_kube_config()
        self.k8s_core_v1 = kubernetes.client.CoreV1Api()
        self.k8s_extensions_v1 = kubernetes.client.ExtensionsV1beta1Api()
        self.k8s_rbac_auth_v1 = kubernetes.client.RbacAuthorizationV1Api()


    def create_gitlab_namespace(self):

        #########################
        # Create gitlab-managed-apps namespace
        #########################
        logger.debug("-" * 30)
        try:
            ns_body = kubernetes.client.V1Namespace()
            ns_body.metadata = kubernetes.client.V1ObjectMeta(name=self.namespace)
            self.k8s_core_v1.create_namespace(ns_body)
        except ApiException as e:
            if eval(e.body)['reason'] == 'AlreadyExists':
                logger.info("Namespace {} already exists".format(self.namespace))
                pass
            else:
                logger.exception(e)
                sys.exit()
        except Exception as e:
            raise e
        logger.debug("-" * 30)

    def create_tiller_serviceaccount(self):

        #########################
        # 1) Create the tiller Service Account
        # core.create_namespaced_service_account("kube-system", serviceaccount_manifest)
        #########################
        logger.debug("-" * 30)
        try:
            logger.debug("Create tiller serviceaccount in {}.".format(self.namespace))
            with open("manifests/tiller/tiller-service-account.yaml") as f:
                serviceaccount_manifest = yaml.load(f, Loader=yaml.FullLoader)
            response = self.k8s_core_v1.create_namespaced_service_account(namespace=self.namespace,
                                                                     body=serviceaccount_manifest)
            logger.debug("RESPONSE: {}".format(response))
        except ApiException as e:
            if eval(e.body)['reason'] == 'AlreadyExists':
                logger.info("Namespaced Service Account already exists")
                pass
            else:
                logger.exception(e)
                sys.exit()
        except Exception as e:
            raise e
        logger.debug("-" * 30)

    def bind_tiller_serviceaccount_to_clusteradmin_role(self):

        #########################
        # 2) Bind the tiller serviceaccount to the cluster-admin role
        # rbac.create_cluster_role_binding(clusterrolebinding_manifest)
        #########################
        logger.debug("-" * 30)
        try:
            logger.debug("Bind tiller serviceaccount to cluster-admin role")
            with open("manifests/tiller/clusterrolebinding.yaml") as f:
                clusterrolebinding_manifest = yaml.load(f, Loader=yaml.FullLoader)
            response = self.k8s_rbac_auth_v1.create_cluster_role_binding(clusterrolebinding_manifest)
            logger.debug("RESPONSE: {}".format(response))
        except ApiException as e:
            if eval(e.body)['reason'] == 'AlreadyExists':
                logger.info("Cluster Role Binding already exists")
                pass
            else:
                logger.exception(e)
                sys.exit()
        except Exception as e:
            raise e
        logger.debug("-" * 30)

    def create_tiller_service(self):

        #########################
        # core.create_namespaced_service("kube-system", service_manifest)
        #########################
        logger.debug("-" * 30)
        try:
            logger.debug("Create Tiller service in {} namespace".format(self.namespace))
            with open("manifests/tiller/tiller-service.yaml") as f:
                service_manifest = yaml.load(f, Loader=yaml.FullLoader)
            response = self.k8s_core_v1.create_namespaced_service(namespace=self.namespace, body=service_manifest)
            logger.debug("RESPONSE: {}".format(response))
        except ApiException as e:
            if eval(e.body)['reason'] == 'AlreadyExists':
                logger.info("Tiller service already exists in {} namespace".format(self.namespace))
                pass
            else:
                logger.exception(e)
                sys.exit()
        except Exception as e:
            raise e
        logger.debug("-" * 30)

    def deploy_tiller(self):
        #########################
        # beta.create_namespaced_deployment("kube-system", deploy_manifest)
        #########################
        logger.debug("-"*30)
        try:
            logger.debug("Create Namespaced Deployment in {}.".format(self.namespace))
            with open("manifests/tiller/tiller-deploy.yaml") as f:
                deploy_manifest = yaml.load(f, Loader=yaml.FullLoader)
            response = self.k8s_extensions_v1.create_namespaced_deployment(namespace=self.namespace, body=deploy_manifest)
            logger.debug("RESPONSE: {}".format(response))
        except ApiException as e:
            if eval(e.body)['reason'] == 'AlreadyExists':
                logger.info("Namespaced Deployment already exists")
                pass
            else:
                logger.exception(e)
                sys.exit()
        except Exception as e:
            raise e
        logger.debug("-" * 30)