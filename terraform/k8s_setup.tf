# EKS requires the usage of Virtual Private Cloud to provide the base for its networking configuration.
# The below will create a 69.0.0.0/16 VPC, two 69.0.X.0/24 subnets, an internet gateway, and setup the subnet routing to route external traffic through the internet gateway:
module "network" {
  source = "./modules/network"

  // pass variables from .tfvars
  region = var.aws_region
  user = var.user
  cluster-name = var.cluster-name
  subnet_count = 2
}

module "security_groups" {
  source = "./modules/security_groups"

  // pass variables from .tfvars
  accessing_computer_ip = var.accessing_computer_ip
  cluster-name = var.cluster-name

  // inputs from modules
  vpc_id = module.network.vpc_id
}