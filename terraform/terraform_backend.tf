terraform {
  backend "s3" {

    bucket         = "jsandlin-aws-tfstate"
    key            = "gitlab_eks_cluster/terraform.tfstate"
    region         = "us-east-2"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "jsandlin_tfstate_locks"
    encrypt        = true
  }
}