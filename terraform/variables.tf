variable "subnet_count" {
  description = "How many subnets do you want to run?"
}
variable "user" {
  description = "Who are you?"
}
variable "cluster-name" {
  description = "name of AWS cluster"
}
variable "aws_region" {
  description = "AWS region"
}
variable "gitlab_namespace" {
  description = "namespace for GitLab's Tiller"
  default = "gitlab-managed-apps"
}
# Data copied from
# https://gitlab.com/groups/sandlin/aws/-/clusters/new?provider=aws
variable "aws_account_id" {
  type = string
}

# Data copied from
# https://gitlab.com/groups/sandlin/aws/-/clusters/new?provider=aws
variable "aws_external_id"{
  type = string
}
variable "accessing_computer_ip" {
  description: "The IP of your work computer, which will have access to the k8s cluster & port 22"
}
//
//output "eks_kubeconfig" {
//  value = local.kubeconfig
//  depends_on = [
//    aws_eks_cluster.master_cluster
//  ]
//}