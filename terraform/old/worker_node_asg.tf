# create a data source to fetch the latest Amazon Machine Image (AMI) that Amazon provides with an EKS compatible Kubernetes baked in
data "aws_ami" "eks-worker" {
   filter {
     name   = "name"
     values = ["amazon-eks-node-${aws_eks_cluster.master_cluster.version}-v*"]
   }

   most_recent = true
   owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

# Create an AutoScaling Launch Config that uses our prerequisite resources to define how to create EC2 instances using them.

//
//# This data source is included for ease of sample architecture deployment
//# and can be swapped out as necessary.
//data "aws_region" "current" {
//}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We implement a Terraform local here to simplify Base64 encoding this
# information into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  cluster-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.master_cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.master_cluster.certificate_authority[0].data}' '${var.cluster-name}'
USERDATA

}
//
//resource "aws_launch_configuration" "tf_eks" {
//  associate_public_ip_address = true
//  iam_instance_profile        = "${aws_iam_instance_profile.node.name}"
//  image_id                    = "${data.aws_ami.eks-worker.id}"
//  instance_type               = "m4.large"
//  name_prefix                 = "terraform-eks"
//  security_groups             = ["${aws_security_group.tf-eks-node.id}"]
//  user_data_base64            = "${base64encode(local.tf-eks-node-userdata)}"
//  key_name                    = "${var.keypair-name}"
//
//  lifecycle {
//    create_before_destroy = true
//  }
//}
resource "aws_launch_configuration" "eks_lc" {
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.node.name
  image_id                    = data.aws_ami.eks-worker.id
  instance_type               = "m4.large"
  name_prefix                 = var.cluster-name
  security_groups  = [aws_security_group.eks_node_sg.id]
  user_data_base64 = base64encode(local.cluster-userdata)

  lifecycle {
    create_before_destroy = true
  }
}


#####################
# Create ASG that actually launches EC2 instances based on teh ASG config
#####################
resource "aws_autoscaling_group" "asg" {
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.eks_lc.id
  max_size             = 2
  min_size             = 1
  name                 = var.cluster-name
  vpc_zone_identifier = [for sn in aws_subnet.mysubnet: sn.id]

  tag {
    key                 = "Name"
    value               = var.cluster-name
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/${var.cluster-name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
