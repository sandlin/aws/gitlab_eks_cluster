# This resource is the actual Kubernetes master cluster.

resource "aws_eks_cluster" "master_cluster" {
  name            = var.cluster-name
  role_arn        = aws_iam_role.cluster.arn

  vpc_config {
    security_group_ids = [aws_security_group.eks_node_sg.id]
    subnet_ids         = [for sn in aws_subnet.mysubnet: sn.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.cluster-AmazonEKSServicePolicy,
  ]
}
