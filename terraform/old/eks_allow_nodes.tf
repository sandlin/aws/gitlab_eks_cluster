#here is one more tricky thing to do: as it is, our worker nodes try to register at our EKS master, but they are not accepted into the cluster. We need to create a config map in our running Kubernetes cluster to accept them. This can be done directly using Kubernetes using the CLI tool kubectl, but you can also use Terraform to do this. You need to set up a new provider for this to address your Kubernetes cluster.
data "external" "aws_iam_authenticator" {
  program = ["sh", "-c", "aws-iam-authenticator token -i ${var.cluster-name} | jq -r -c .status"]
}

provider "kubernetes" {
  host                      = aws_eks_cluster.master_cluster.endpoint
  cluster_ca_certificate    = base64decode(aws_eks_cluster.master_cluster.certificate_authority.0.data)
  token                     = data.external.aws_iam_authenticator.result.token
  load_config_file          = false
  version = "~> 1.5"
}


#EKS integrates with Amazon’s account and permission services, which means that you need an AWS IAM token to connect to the master. To obtain the token, we use the AWS CLI tool and define a command as a data source. For this to work, make sure that the AWS CLI tool is set up on your computer, including the credentials required to connect to the AWS account containing the EKS master. We then use the token and some information from our EKS master to set up the provider connecting to Kubernetes. Then we use it to create the mentioned config map, allowing the IAM role created for our nodes into the cluster:

resource "kubernetes_config_map" "aws_auth" {
  metadata {
    name = "${var.cluster-name}-aws-auth"
    namespace = "kube-system"
  }
  data = {
    mapRoles = <<EOF
- rolearn: aws_iam_role.iam_role_eks_worker_nodes.arn
  username: system:node:{{EC2PrivateDNSName}}
  groups:
    - system:bootstrappers
    - system:nodes
EOF
  }
  depends_on = [
    aws_eks_cluster.master_cluster
  ]
}