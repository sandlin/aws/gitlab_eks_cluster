
# https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform_part2/
resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_block

  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Owner = var.user
    #"kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
