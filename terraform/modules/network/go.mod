module test

go 1.14

require (
	github.com/gruntwork-io/terratest v0.27.1
	github.com/stretchr/testify v1.5.1
)
