
# Route Tables make sure instances in the subnetes use the gateways when attempting to access internet.
#   We create a Route Table for each subnet, making sure that our gateway subnets use the previously built Internet Gateway
#   to allow connections in and out, and the app subnets use the NAT Gateways to connect to the internet.
resource "aws_route_table" "app_rt" {
  count = var.subnet_count
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.myngw.*.id[count.index]
  }
  tags = {
    Owner = var.user
    Name = "${var.cluster-name}-app-rt"
  }
}

resource "aws_route_table" "db_rt" {

  vpc_id = aws_vpc.vpc.id

  tags = {
    Owner = var.user
    Name = "${var.cluster-name}-db-rt"
  }
}
resource "aws_route_table" "gateway_rt" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myigw.id
  }
  tags = {
    Owner = var.user
    Name = "${var.cluster-name}-gw-rt"
  }
}

# Now we need to associate the route tables with the subnets.
resource "aws_route_table_association" "app_rta" {
  count = var.subnet_count

  subnet_id = aws_subnet.app.*.id[count.index]
  route_table_id = aws_route_table.app_rt.*.id[count.index]
}

resource "aws_route_table_association" "db_rta" {
  count = var.subnet_count

  subnet_id = aws_subnet.db.*.id[count.index]
  route_table_id = aws_route_table.db_rt.id
}

resource "aws_route_table_association" "gateway_rta" {
  count = var.subnet_count

  subnet_id = aws_subnet.gateway.*.id[count.index]
  route_table_id = aws_route_table.gateway_rt.id
}

