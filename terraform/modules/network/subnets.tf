
# We use a data source type provided by the AWS provider to fetch a list of available Availability Zones for the region we use with our AWS provider.
data "aws_availability_zones" "available" {
}
# One gateway subnet, which contains an internet gateway that can be used to connect to outside resources and receive traffic from the internet
resource "aws_subnet" "gateway" {
  count = var.subnet_count
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = "69.0.1${count.index}.0/24"
  vpc_id            = aws_vpc.vpc.id
  tags = {
    Owner = var.user
    Name = "${var.cluster-name}_gateway"
  }
}
# One app subnet, which will contain our EKS cluster and other resources related to our hosted services and servers
resource "aws_subnet" "app" {
  count = var.subnet_count
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = "69.0.2${count.index}.0/24"
  vpc_id            = aws_vpc.vpc.id
  tags = {
    Owner = var.user
    Name = "${var.cluster-name}_app"
    "kubernetes.io/cluster/${var.cluster-name}" = "shared"
  }
}
# One db subnet, which should only be accessed from our app subnet to make sure that our data is safe and basically impossible to access from the outside.
resource "aws_subnet" "db" {
  count = var.subnet_count
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = "69.0.3${count.index}.0/24"
  vpc_id            = aws_vpc.vpc.id

  tags = {
    Owner = var.user
    Name = "${var.cluster-name}_db"
  }
}
