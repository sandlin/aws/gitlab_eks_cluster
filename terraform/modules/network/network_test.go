package network

import (
	"fmt"
	"github.com/gruntwork-io/terratest/modules/retry"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"math/rand"
	"testing"
	"time"

	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/gruntwork-io/terratest/modules/terraform"
)


func setConfig(t *testing.T, awsRegion string, subnetCount int) *terraform.Options{
	//r := rand.New(time.Now().UnixNano())
	rand.Seed(time.Now().UnixNano())

	randVal := rand.Intn(99)
	testUser := fmt.Sprint("network_test", randVal)
	testClusterName := fmt.Sprint("network_test", randVal)

	terraformOptions := &terraform.Options{
		TerraformDir: ".",

		// Variables to pass to our Terraform code using -var options
		Vars: map[string]interface{}{
			"user": testUser,
			"cluster-name": testClusterName,
			"region": awsRegion,
			"subnet_count": subnetCount,
		},
	}
	return terraformOptions
}

// We should test VPC, Subnets, Route Tables and Gateways
// https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform_part2/
func TestNetworkSetup(t *testing.T) {
	t.Parallel()
	subnetCount := 2
	maxRetries := 5
	/*
	 * When doing basic network testing, it's common to receive VpcLimitExceeded
	 * `Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached.`
	 * Therefore, catch it and retry so we hit another region.
	 */
	retryDescription := "Return value after 5 retries with expected error, match regex"
	retryableErrors := map[string]string{
		".*VpcLimitExceeded.*": "Error creating VPC: VpcLimitExceeded: The maximum number of VPCs has been reached. Try another region.",
		".*AddressLimitExceeded.*": "Error: Error creating EIP: AddressLimitExceeded: The maximum number of addresses has been reached.",
		".*NatGatewayLimitExceeded.*": "Error creating NAT Gateway: NatGatewayLimitExceeded: Performing this operation would exceed the limit of 5 NAT gateways",
	}
	actualOutput, err := retry.DoWithRetryableErrorsE(t, retryDescription, retryableErrors, maxRetries, 1*time.Millisecond, func() (string, error) {
		// Pick a random AWS region to test in. This helps ensure your code works in all regions.
		awsRegion := aws.GetRandomStableRegion(t, nil, nil)

		terraformOptions := setConfig(t, awsRegion, subnetCount)

		defer terraform.Destroy(t, terraformOptions)

		terraform.Init(t, terraformOptions)
		println("-------------------------")
		fmt.Println(terraformOptions.Vars)
		println("-------------------------")
		output, err := terraform.ApplyE(t, terraformOptions)
		//assert.Equal(t, expectedOutput, actualOutput)
		// Run `terraform output` to get the value of an output variable
		vpcId := terraform.Output(t, terraformOptions, "vpc_id")
		newVpc := aws.GetVpcById(t, vpcId, awsRegion)
		assert.Equal(t, newVpc.Id, vpcId)
		appSubnetIds := terraform.OutputList(t, terraformOptions, "app_subnet_ids")
		dbSubnetIds := terraform.OutputList(t, terraformOptions, "db_subnet_ids")
		gatewaySubnetIds := terraform.OutputList(t, terraformOptions, "gateway_subnet_ids")
		subnets := aws.GetSubnetsForVpc(t, vpcId, awsRegion)

		// Ensure we have 3 subnets
		require.Len(t, subnets, (subnetCount * 3), "Your subnet count should be %q; but it's %q", (subnetCount*3), len(subnets))

		require.Len(t, appSubnetIds, subnetCount,"Your app subnet count should be %q; but it's %q", subnetCount, len(appSubnetIds))
		require.Len(t, gatewaySubnetIds, subnetCount,"Your gateway subnet count should be %q; but it's %q", subnetCount, len(gatewaySubnetIds))
		require.Len(t, dbSubnetIds, subnetCount,"Your app DB subnet count should be %q; but it's %q", subnetCount, len(dbSubnetIds))


		// Verify the app subnet is not public
		for _, sn := range appSubnetIds {
			assert.False(t, aws.IsPublicSubnet(t, sn, awsRegion))
		}
		// Verify the DB subnet is private.
		for _, sn := range dbSubnetIds {
			assert.False(t, aws.IsPublicSubnet(t, sn, awsRegion))
		}
		// Verify the gateway network is public
		for _, sn := range gatewaySubnetIds {
			assert.True(t, aws.IsPublicSubnet(t, sn, awsRegion))
		}
		return output, err
	})
	if err != nil {
		println("-------------------------------")
		println("ERROR CREATING YOUR NETWORK. WE FAILED 5 TIMES DUE TO USAGE")
		println(actualOutput)
		println("-------------------------------")
		println(err)
		println("-------------------------------")
	}
}
