# REQUIRED FOR VPC
variable "user" {
  description = "Who are you?"
}
variable "region" {
  description = "AWS region"
}
variable "cluster-name" {
  description = "name of AWS cluster"
}
variable "subnet_count" {
  description = "Number of subnets to spin up"
}
variable "cidr_block" {
  default = "69.0.0.0/16"
  description = "The CIDR block for your VPC"
}

//variable "cidr_block" {
//  default = "69.0.0.0/24"
//  description = "The CIDR block for your VPC"
//}
//variable "app_subnets" {
//  description = "Private"
//  default = "69.0.2"
//}
//variable "db_subnets" {
//  description = "Private"
//  default = "69.0.3"
//}
//variable "gateway_subnets" {
//  description = "Public"
//  default = "69.0.1"
//}
