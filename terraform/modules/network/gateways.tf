
# Configure a way for us to be able to connect to the internet.
resource "aws_internet_gateway" "myigw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Owner = var.user
    Name = var.cluster-name
  }
}

# To address these from each subnet, we also need to create a Elastic IP Address every time
#   – these are unchanging IP’s that we can associate with our resources to make sure they are always available at the same endpoint.
resource "aws_eip" "nat_gateway_eip" {
  count = var.subnet_count
  vpc      = true
}


# Create NAT Gateway
#   - to ensure db subnets can connect to net w/o being accessable from the internet
resource "aws_nat_gateway" "myngw" {
  count = var.subnet_count
  allocation_id = aws_eip.nat_gateway_eip.*.id[count.index]
  subnet_id = aws_subnet.gateway.*.id[count.index]
  tags = {
    Owner = var.user
    Name = var.cluster-name
  }
  depends_on = [aws_internet_gateway.myigw]
}
