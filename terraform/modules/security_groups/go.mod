module security_groups

go 1.14

require (
	github.com/aws/aws-sdk-go v1.27.1
	github.com/gruntwork-io/terratest v0.27.2
	github.com/magiconair/properties v1.8.1
	github.com/stretchr/testify v1.4.0
)
