# This security group controls networking access to the Kubernetes masters.
resource "aws_security_group" "eks_master_sg" {
  name        = "${var.cluster_name}_master_sg"
  description = "Cluster communication with worker nodes"
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "${var.cluster_name}_master_sg"
  }
}
