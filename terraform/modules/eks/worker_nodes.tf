########################################################################################
# Setup AutoScaling Group for worker nodes

# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv

# Setup data source to get amazon-provided AMI for EKS nodes
data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks_node-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID
}

# Is provided in demo code, no idea what it's used for though! TODO: DELETE
# data "aws_region" "current" {}

# EKS currently documents this required userdata for EKS worker nodes to
# properly configure Kubernetes applications on the EC2 instance.
# We utilize a Terraform local here to simplify Base64 encode this
# information and write it into the AutoScaling Launch Configuration.
# More information: https://docs.aws.amazon.com/eks/latest/userguide/launch-workers.html
locals {
  eks_node-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.eks_cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.eks_cluster.certificate_authority.0.data}' 'example'
USERDATA
}

resource "aws_launch_configuration" sg_eks_master {
  associate_public_ip_address = true
  iam_instance_profile        = aws_iam_instance_profile.node.name
  image_id                    = data.aws_ami.eks-worker.id
  instance_type               = "m4.large"
  name_prefix                 = "terraform-eks"
  security_groups             = [aws_security_group.eks_node.id]
  user_data_base64            = base64encode(local.eks_node-userdata)
  key_name                    = var.keypair-name

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" sg_eks_master {
  desired_capacity     = "2"
  launch_configuration = aws_launch_configuration.tf_eks.id
  max_size             = "3"
  min_size             = 1
  name                 = "terraform-tf-eks"
  vpc_zone_identifier  = [var.app_subnet_ids]

  tag {
    key                 = "Name"
    value               = "terraform-tf-eks"
    propagate_at_launch = true
  }

  tag {
    key                 = "kubernetes.io/cluster/example"
    value               = "owned"
    propagate_at_launch = true
  }
}
