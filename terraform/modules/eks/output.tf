
# src: https://www.esentri.com/building-a-kubernetes-cluster-on-aws-eks-using-terraform-part-iv
output "eks_kubeconfig" {
  value = local.kubeconfig
  depends_on = [
    "aws_eks_cluster.eks_cluster."
  ]
}
