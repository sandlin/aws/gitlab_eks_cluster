import logging
import warnings
import os
import hcl
import sys

import warnings

warnings.simplefilter("ignore")
warnings.filterwarnings(action='ignore', module='hcl')

def import_tf_cfg():
    # Read the cluster name from the variable file.
    cfg_file = os.path.join(os.getcwd(), 'terraform', 'variables.tf')

    with open(cfg_file, 'r') as fp:
        tfcfg = hcl.load(fp)
        fp.close()
    return(tfcfg)


def main(argv):
    tfcfg = import_tf_cfg()
    eks_cluster_name = tfcfg['variable']['cluster-name']['default']
    aws_region = tfcfg['variable']['aws_region']['default']
    print("-"*30)
    print("Ignore token WARNINGs above. Executing the following:")
    print("    aws eks update-kubeconfig --region {r} --name {n} --alias {n}".format(r=aws_region, n=eks_cluster_name))
    print("-"*30)
    os.system("aws eks update-kubeconfig --region {r} --name {n} --alias {n}".format(r=aws_region, n=eks_cluster_name))

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
