# PREREQ:
1. Create a file in the `terraform` directory called `$HOME/.aws/.tfvars`. 
    ```
    # You need your AWS Account ID
    aws_account_id = "__________________"
    
    
    # You need your AWS External ID
    # https://gitlab.com/groups/sandlin/aws/-/clusters/new?provider=aws
    aws_external_id = "____________________________________"
    ```
1. Modify the `terraform/variables.tf` file per your settings
1. Install required software
    1. MacOS
        1. aws-iam-authenticator
         ```shell script
            brew install aws-iam-authenticator
         ```
        1. jq
         ```shell script
            brew install jq
         ```
        1. terraform
         ```shell script
            brew install terraform
         ```
# CREATE THE CLUSTER
```shell script
./1_create_cluster.sh
```

# Redirect kubectl
This script will modify your local Kubectl to point to the cluster created above.
Ignore the WARNINGs at the top. They are due to LEX/YACC reading the tf config.
```shell script
python update-kubeconfig-to-cluster.py
```
EX:
```shell script
➜ python update-kubeconfig-to-cluster.py
WARNING: Token 'COMMAEND' defined, but not used
WARNING: There is 1 unused token
------------------------------
Ignore token WARNINGs above. Executing the following:
    aws eks update-kubeconfig --region us-east-2 --name jsandlin-eks
------------------------------
Updated context arn:aws:eks:us-east-2:871730672732:cluster/jsandlin-eks in /Users/jsandlin/.kube/config

Process finished with exit code 0
```


# SETUP
[SOURCE](https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#existing-eks-cluster)

1. Get the service account token for integration.
```shell script
➜ kubectl get secrets | grep "service-account-token" | grep -G "^default-token" | awk '{print $1}'
default-token-foo
```

```shell script
kubectl get secret `kubectl get secrets | grep "service-account-token" | grep -G "^default-token" | awk '{print $1}'` -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```


# Python Script
1. Setup your auth token @ https://gitlab.com/profile/personal_access_tokens
   1. I granted my token the following Scopes: `api` `write_repository` `read_registry`
   1. Copy the token generated as it will not be accessable once you leave the page
1. Create your python-gitlab auth / config file `~/.python-gitlab.cfg`
   1. Replace the private_token value with the value received above.
   1. Your file will look like: 
        ```shell script
        # REF: https://python-gitlab.readthedocs.io/en/stable/cli.html#configuration
        [global]
        default = com
        ssl_verify = true
        timeout = 5
        
        [com]
        url = https://gitlab.com
        private_token = xxxxxxxxxxx
        api_version = 4
        ```
