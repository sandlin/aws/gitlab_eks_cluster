import unittest
import argparse
import configparser
import logging
import os
import re
import sys
import json
import base64
from pathlib import Path
import yaml
import boto3
import gitlab
import hcl
import jinja2
from pprint import pprint
import kubernetes
from kubernetes.client.rest import ApiException

import kube_apply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())


class TillerMgrValidator(unittest.TestCase):
    '''
    TillerMgtTest is doing post deployment validation.
    '''
    def setUp(self):
        self.import_tf_cfg()
        kubernetes.config.load_kube_config()
        self.k8sapi = kubernetes.client.CoreV1Api()

    def import_tf_cfg(self):
        # Read the cluster name from the variable file.
        cfg_file = os.path.join(os.getcwd(), 'terraform', 'variables.tf')

        with open(cfg_file, 'r') as fp:
            tfcfg = hcl.load(fp)
            fp.close()
        self.variables = tfcfg['variable']

    def create_gitlab_namespace_test(self):
        #########################
        # Create gitlab-managed-apps namespace
        #########################
        myns = self.k8sapi.list_namespace(field_selector="metadata.name={}".format(self.variables["gitlab_namespace"]["default"]))
        self.assertIsInstance(myns, kubernetes.client.V1NamespaceList)
        self.assertEqual(myns.items[0].metadata.name, self.variables["gitlab_namespace"]["default"])

    def create_tiller_serviceaccount_test(self):
        #########################
        # 1) Create the tiller Service Account
        # core.create_namespaced_service_account("kube-system", serviceaccount_manifest)
        #########################
        # https://github.com/kubernetes-client/python/blob/master/kubernetes/docs/CoreV1Api.md#read_namespaced_service_account
        mysa = self.k8sapi.read_namespaced_service_account(name="tiller", namespace=self.variables["gitlab_namespace"]["default"])
        self.assertIsInstance(mysa, kubernetes.client.V1ServiceAccount)
        self.assertEqual(mysa.metadata.namespace, self.variables["gitlab_namespace"]["default"])
        self.assertEqual(mysa.metadata.name, "tiller")

    def bind_tiller_serviceaccount_to_clusteradmin_role_test(self):

