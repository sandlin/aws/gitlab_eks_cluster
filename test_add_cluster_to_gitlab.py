from unittest import TestCase
from argparse import ArgumentError
import 'add_cluster_to_gitlab' as script

class TestTillerMgr(TestCase):

    def test_parse_args(self):

        parsed = script.parse_args(['--pid=22'])
        self.assertEqual(parsed.pid, 22)
        parsed = script.parse_args(['--gid=22'])
        self.assertEqual(parsed.gid, 22)

        parsed = script.parse_args(['--namespace=22', '--pid=33'])
        self.assertEqual(parsed.namespace, "22")

    def test_apply_yaml(self):
        with self.assertRaises(UnboundLocalError):
            script.apply_yaml("badfilename.txt")


