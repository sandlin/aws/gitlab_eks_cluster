#!/bin/bash
if [[ ! -f "$HOME/.aws/.tfvars" ]]
then
  echo "Cannot find .tfvars in " + pwd
  return 1
fi
PLAN_FILE=myplan.tfplan
PLAN_JSON=myplan.json
# Create the EKS cluster
cd terraform
terraform init
rm -f $PLAN_FILE $PLAN_JSON
terraform plan -var-file="$HOME/.aws/.tfvars" -out $PLAN_FILE
terraform show -json $PLAN_FILE > $PLAN_JSON
terraform apply -var-file="$HOME/.aws/.tfvars" --auto-approve

echo "Now it's time for you to configure your local kubectl to point to this new cluster."
