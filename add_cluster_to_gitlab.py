import argparse
import configparser
import logging
import os
import re
import sys
from pythonlib.TillerMgr import TillerMgr
import json
import base64
from pathlib import Path
from pprint import pprint
import yaml
import boto3
import gitlab
import hcl
import jinja2
import kubernetes as k8s
from kubernetes.client.rest import ApiException

import kube_apply

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

class EksCluster():

    def __init__(self, name, k8s_api_url, k8s_token, k8s_ca_cert):
        self.name = name
        self.platform_kubernetes_attributes = {
            'api_url': k8s_api_url,
            'token': k8s_token,
            'ca_cert': k8s_ca_cert
        }

def import_tf_cfg():
    # Read the cluster name from the variable file.
    cfg_file = os.path.join(os.getcwd(), 'terraform', 'variables.tf')

    with open(cfg_file, 'r') as fp:
        tfcfg = hcl.load(fp)
        fp.close()
    return(tfcfg['variable'])

def apply_yaml(filename: str):
    """kubectl apply -f <filename>

    :param filename: The yaml file you wish to apply via kubectl
    :type filename: str
    """
    try:
        with open(filename, 'r') as fp:
            kube_apply.fromYaml(fp)
    except FileNotFoundError as e:
        raise
    except UnboundLocalError as e:
        raise
    finally:
        fp.close()
#
# def get_ca_cert(name_substr: str, namespace: str = "default") -> [str, str]:
#     """https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#existing-eks-cluster
#     1.1 - Retrieve the certificate
#
#     Translates to cmd:
#         kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
#
#     :param name_substr: A substring of the service account for which you wish to retrieve the token.
#     :type name_substr: str
#     :param namespace: The namespace from which to pull the cert
#     :type namespace: str
#
#     :return: secret_name, secret_certificate
#     note:: This is expecting a single secret to exist in the provided namespace.
#     """
#     k8s.config.load_kube_config()
#     k8sapi = k8s.client.CoreV1Api()
#     secrets = k8sapi.list_namespaced_secret(namespace=namespace)
#
#     for s in secrets.items:
#         if re.match(name_substr, s.metadata.name):
#             decoded_cert = base64.b64decode(s.data['ca.crt']).decode('utf8')
#             secret_name = s.metadata.name
#     return(secret_name, decoded_cert)
#

def get_ca_cert(namespace: str = "default") -> [str, str]:
    """https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#existing-eks-cluster
    1.1 - Retrieve the certificate

    Translates to cmd:
        kubectl get secret $(kubectl get secrets | grep default-token | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

    :param namespace: The namespace from which to pull the cert
    :return: secret_name, secret_certificate
    note:: This is expecting a single secret to exist in the provided namespace.
    """
    k8s.config.load_kube_config()
    k8sapi = k8s.client.CoreV1Api()
    secrets = k8sapi.list_namespaced_secret(namespace=namespace)
    if len(secrets.items) > 1:
        logger.warning(
            "You have {} secrets.\n   At this point there should only be 1.\n   If you have set things up manually / differently, this automation may fail.".format(
                len(secrets.items)))
        sys.exit(1)
    mysecret = secrets.items[0]
    decoded_cert = base64.b64decode(mysecret.data['ca.crt']).decode('utf8')
    secret_name = mysecret.metadata.name
    return(secret_name, decoded_cert)

def get_token_n_cert(name_substr: str, namespace: str) -> [str, str]:
    """
    Query the provided namespace for the secret matching the provided name substring. Retrieve its token.

    Translates to cmd:
        kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')

    :param name_substr: A substring of the service account for which you wish to retrieve the token.
    :type name_substr: str
    :param namespace: The namespace under which you wish to look for said account.
    :type namespace: str

    :raises: :class: `NameError`: The name_substr arg you provided is not found.

    :returns: Name of token matching name_substr && token associated with this name prefix.
    """
    k8s.config.load_kube_config()
    k8sapi = k8s.client.CoreV1Api()
    secrets = k8sapi.list_namespaced_secret(namespace=namespace)
    for s in secrets.items:
        if re.match(name_substr, s.metadata.name):
            # We found the secret matching the name substring.
            decoded_cert = base64.b64decode(s.data['ca.crt']).decode('utf8')
            decoded_token = base64.b64decode(s.data['token']).decode('utf8')
            return s.metadata.name, decoded_token, decoded_cert
    logger.exception("Unable to find any secrets matching the substring {} in namespace {}".format(name_substr, namespace))
    raise NameError("Unable to find any secrets matching the substring {} in namespace {}".format(name_substr, namespace))


def parse_args(args):
    """
    Parse command line arguments

    :param args: command args

    :returns: argparse args
    """
    parser = argparse.ArgumentParser(description="Configure GitLab with your AWS Cluster")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--pid', type=int, help='The ID of the project to link your EKS')
    group.add_argument('--gid', type=int, help='The ID of the group to link your EKS')
    #parser.add_argument("--namespace", required=False, help='If you would like to define your namespace. Otherwise, it will be the cluster-name from your variables.')
    pargs = parser.parse_args(args)
    return pargs


def add_cluster_to_gitlab(**kwargs):
    """

    """
    ekscluster = kwargs.get('ekscluster')
    pid = kwargs.get('pid')
    gid = kwargs.get('gid')

    # Now let's start talking to GitLab
    cp = configparser.ConfigParser()
    cp.read(os.path.join(str(Path.home()), '.python-gitlab.cfg'))
    gl = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])

    gl = gitlab.Gitlab(cp['com']['url'], private_token=cp['com']['private_token'])
    # Need to put testing, try/catch, etc here.
    # Are we adding to a group or project?


    if pid is not None:
        try:
            myproject = gl.projects.get(pid)
            # Is cluster already in the group?
            for c in myproject.clusters.list():
                if c.name == ekscluster.name:
                    print("Project Cluster {} already exists. It was created at {}".format(c.name, c.created_at))
                    return(c)
            cluster = myproject.clusters.create(ekscluster.__dict__)
            return(cluster)
        except gitlab.GitlabCreateError as e:
            logger.exception(e)
            raise(e)
    if gid is not None:
        try:
            mygroup = gl.groups.get(gid)
            # Is cluster already in the group?
            for c in mygroup.clusters.list():
                if c.name == ekscluster.name:
                    print("Group Cluster {} already exists. It was created at {}".format(c.name, c.created_at))
                    return(c)
            cluster = mygroup.clusters.create(ekscluster.__dict__)
            return(cluster)
        except gitlab.GitlabCreateError as e:
            logger.exception(e)
            raise(e)

def create_gitlab_namespace():


def tiller_install(namespace):
    '''

    https://github.com/JulienBalestra/enjoliver/blob/master/app/tests/euid/kvm_player.py
    https://www.programcreek.com/python/example/96328/kubernetes.client.CoreV1Api
    https://www.terraform.io/docs/providers/helm/index.html
    https://github.com/kubernetes-client/python/blob/master/examples/deployment_create.py
    https://www.digitalocean.com/community/tutorials/how-to-install-software-on-kubernetes-clusters-with-the-helm-2-package-manager
    '''

    '''
    1) Create the tiller serviceaccount
    2) Bind the tiller serviceaccount to the cluster-admin role
    3) Install Tiller on our cluster, along with some local housekeeping tasks such as downloading the stable repo details.
    '''

    tm = TillerMgr()
    tm.create_gitlab_namespace()
    # 1) Create the tiller serviceaccount
    tm.create_tiller_serviceaccount()
    # 2) Bind the tiller serviceaccount to the cluster-admin role
    tm.bind_tiller_serviceaccount_to_clusteradmin_role()
    # 3) Install Tiller on our cluster, along with some local housekeeping tasks such as downloading the stable repo details.
    tm.create_tiller_service()
    tm.deploy_tiller()



def main(**kargs):
    tfcfg = import_tf_cfg()

    # # https://docs.gitlab.com/ee/user/project/clusters/add_eks_clusters.html#existing-eks-cluster
    # # 1.1 - Retrieve the certificate.
    default_secret_name, ca_token, ca_cert = get_token_n_cert(name_substr="default-token", namespace="default")
    # # 1.2 - Create admin token
    # # 1.2.1 file exists in kubeconfig dir.
    # # 1.2.2 - Apply the service account to the cluster.
    apply_yaml(filename='kubeconfig/eks-admin-service-account.yaml')
    # # 1.2.3 file exists in kubeconfig dir
    # # 1.2.4 - Apply the cluster role binding to the cluster.
    apply_yaml(filename='kubeconfig/eks-admin-cluster-role-binding.yaml')
    # 1.2.5 - Retrieve the token for the eks-admin service account.
    #    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
    kubesystem_secret_name, kubesystem_token, ks_cert = get_token_n_cert(name_substr="eks-admin", namespace="kube-system")
    # 1.3 - Locate the API server endpoint so GitLab can connect to the cluster
    eks_client = boto3.client('eks')
    cluster_details = eks_client.describe_cluster(name=tfcfg['cluster-name']['default'])
    api_server_endpoint = cluster_details['cluster']['endpoint']
    # WE NOW HAVE ALL OUR INFORMATION & CAN CONFIGURE GITLAB.

    ekscluster = EksCluster(name=tfcfg['cluster-name']['default'], k8s_api_url=api_server_endpoint, k8s_token=kubesystem_token, k8s_ca_cert=ca_cert)
    # Send required json to GL to add the cluster.
    glcluster = add_cluster_to_gitlab(ekscluster=ekscluster, **kargs)
    # Now it's time to ensure appropriate software is installed.
    tiller_install(namespace=tfcfg['gitlab_namespace']['default'])


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    sys.exit(main(**vars(args)))

